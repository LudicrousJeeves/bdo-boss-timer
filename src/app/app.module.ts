import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BossTableComponent } from './boss-table/boss-table.component';
import { BossComponent } from './boss/boss.component';

@NgModule({
  declarations: [
    AppComponent,
    BossTableComponent,
    BossComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
