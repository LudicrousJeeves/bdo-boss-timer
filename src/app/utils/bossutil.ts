import { Boss, WeeklyRotation, BossResult, BossForTime, BossArray } from '../models';
import { getCurrentUTCSeconds, Day, getUTCDayOfWeek } from './dateutil';
import { DataService } from '../data.service';
import { element } from 'protractor';
import { ArrayType, BoundDirectivePropertyAst } from '@angular/compiler';
import { ɵBROWSER_SANITIZATION_PROVIDERS } from '@angular/platform-browser';

//this.secondsIndex = [900, 7200, 18000, 32400, 43200, 57600, 68400, 80100, 83700];
// export const getNextBoss = (bossRotation: WeeklyRotation, secondsList: number[]) : BossResult => {
//     const seconds = getCurrentUTCSeconds();
//     let day = new Date().getUTCDay();
//     let dayOfWeek = getCurrentUTCDay(day);

//     let nextBossIndex: number;
//     if(seconds > secondsList[secondsList.length - 1]) {
//         if(++dayOfWeek === 7) {
//             dayOfWeek = 0;
//         }
//         nextBossIndex = 0;
//     } else {
//         nextBossIndex = secondsList.findIndex(second => seconds < second);
//     }

//     return {
//         previous: getPreviousBoss(dayOfWeek, nextBossIndex),
//         next: bossRotation[dayOfWeek][nextBossIndex],
//         followedBy: getFollowedByBoss(dayOfWeek, nextBossIndex)
//     }

//     function getPreviousBoss(dayOfWeek: number, nextBossIndex: number): BossForTime {
//         if(dayOfWeek === 0 && nextBossIndex === 0) {
//             return bossRotation[6][secondsList.length - 1]
//         } else if (nextBossIndex === 0) {
//             return bossRotation[dayOfWeek - 1][secondsList.length - 1]
//         }
//         return bossRotation[dayOfWeek][nextBossIndex - 1]
//     }

//     function getFollowedByBoss(dayOfWeek: number, nextBossIndex: number): BossForTime {
//         if(dayOfWeek === 6 && nextBossIndex === secondsList.length - 1) {
//             return bossRotation[0][0]
//         } else if (nextBossIndex === secondsList.length - 1) {
//             return bossRotation[dayOfWeek + 1][0]
//         }
//         return bossRotation[dayOfWeek][nextBossIndex + 1]
//     }
// }

export const getTodaysBosses = (bosses: Object[]) => {
    let day = new Date().getUTCDay();
    let dayOfWeek = getUTCDayOfWeek(day); 
    function bossesToday(boss) {
        if(boss.Time[day].Day === dayOfWeek && boss.Time[day].Seconds.length != 0) {
            return boss;
        }
    }
    return bosses.filter(bossesToday); 
}

export const getNextBoss = (bosses: Object[]) => {
    const seconds = getCurrentUTCSeconds();
    let bossesToday = getTodaysBosses(bosses);
    let day = new Date().getUTCDay();
    let dayOfWeek = getUTCDayOfWeek(day);
    const array = new Array();
    console.log(bossesToday);

    function nextBoss(boss: Object[]){
        for(let i = 0; i < boss.Time[day].Seconds.length; i++) {
            if(seconds < boss.Time[day].Seconds[i]) {
                return boss
            }
        }
    }

    return bossesToday.filter((element) => element.Time[day].Seconds < seconds);

    //console.log(bosses.find(bossesToday));

    // return {
    //     previous: getPreviousBoss(),
    //     next: ,
    //     followedBy: getFollowedByBoss()
    // }
}