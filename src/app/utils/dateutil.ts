export const getCurrentUTCSeconds = () : number => {
    const today = new Date();
    today.setHours(6);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    return (new Date().getTime() - today.getTime()) / 1000;
};

export const getUTCDayOfWeek = (day : number) : string => {
    const dayOfWeek = Day[day];
    return dayOfWeek;
}

export enum Day {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
}