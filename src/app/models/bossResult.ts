export interface BossResult {
    previous: BossForTime;
    next: BossForTime;
    followedBy: BossForTime;
}

export type BossForTime = string | string[]