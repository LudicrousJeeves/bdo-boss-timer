export interface BossRotation {
    [key: string] : WeeklyRotation
}

export type WeeklyRotation = Array<Array<(string | Array<string>)>>