export interface BossArray {
    boss: Boss[]
}

export interface Boss {
    name: string,
    description: string,
    image: string,
    time: Time[]
}

export interface Time {
    day: string,
    time: number
}