import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { getCurrentUTCSeconds } from '../utils/dateutil'; 

@Component({
  selector: 'app-boss',
  templateUrl: './boss.component.html',
  styleUrls: ['./boss.component.css']
})
export class BossComponent implements OnInit {
  displayBosses: any;
  serverBossRotation: any;

  constructor(dataService: DataService) {
    // setInterval(() => {
    //   this.time = new Date();
    //   if(this.time.getHours() > 22 && this.time.getMinutes() > 15)
    //   {
    //     this.time = new Date().getDate()+1;
    //     this.displayBosses = dataService.getNextBoss(dataService.getBossFromArray(this.time, this.serverBossRotation));
    //   }
    //   else
    //   {
    //     this.displayBosses = dataService.getNextBoss(dataService.getBossFromArray(this.time, this.serverBossRotation));
    //     console.log(this.displayBosses)
    //   }
    // }, 150000);
  }

  isArray(item){
    return Array.isArray(item);
  }

  ngOnInit(): void {
  }

}
