import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Time } from '@angular/common';
import { BossRotation, BossResult } from "./models"
import { getNextBoss, getTodaysBosses} from './utils';

enum Index {
  "00:00", 
  "03:00", 
  "07:00", 
  "10:00", 
  "14:00", 
  "17:00", 
  "20:15", 
  "21:15", 
  "22:15"
}

@Injectable({
  providedIn: 'root'
})

export class DataService {
  bosses: any;
  weekDay: any;
  daysOfTheWeek: any;
  bossRotation: BossRotation;
  naUTCTimes: any;
  secondsIndex: any;
  todaysBosses: any;
  bossesByTime: BossResult = {
    next: "",
    previous: "",
    followedBy: ""
  }


  constructor() { 
    this.daysOfTheWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    this.naUTCTimes = ["UTC", "08:00", "11:00", "15:00", "18:00", "22:00", "01:00", "04:15", "05:15", "06:15"];
    //this.secondsIndex = [28800, 39600, 54000, 64800, 79200, 3600, 15300, 18900, 22500];
    //Seconds of the day that refers to UTC time
    this.secondsIndex = [3600, 15300, 18900, 22500, 28800, 39600, 54000, 64800, 79200]
    this.bossRotation = {
      na: [["Kzarka", "Kutum", "Nouver", "Kzarka", "Vell", "Garmoth", ["Kzarka", "Nouver"], "", ["Karanda", "Kutum"]],
      ["Karanda", "Kzarka", "Kzarka", "Offin", "Kutum", "Nouver", "Kzarka", "", "Karanda"],
      ["Kutum", "Kzarka", "Nouver", "Kutum", "Nouver", "Karanda", "Garmoth", "", ["Kutum", "Kzarka"]],
      ["Karanda", "", "Karanda", "Nouver", "Kutum", "Offin", ["Kzarka", "Karanda"], ["Quint", "Muraka"], "Nouver"],
      ["Kutum", "Kzarka", "Kutum", "Nouver", "Kzarka", "Kutum", "Garmoth", "", ["Kzarka", "Karanda"]],
      ["Nouver", "Karanda", "Kutum", "Karanda", "Nouver", "Kzarka", ["Kutum", "Kzarka"], "", "Karanda"],
      ["Offin", "Nouver", "Kutum", "Nouver", ["Quint", "Muraka"], ["Kzarka", "Karanda"], "", "", ["Nouver", "Kutum"]]
    ],
      eu: [],
      as: []
    }

    //this.bossRotation['na']
    //console.log(this.bossRotation['na']);

    // const bosses = getNextBoss(this.bossRotation['na'], this.secondsIndex);
    // Object.assign(this.bossesByTime, bosses);
    
    this.bosses = [
      {
        Name: "Karanda",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/karanda.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [22500, 28800]
          },
          {
            Day: "Monday",
            Seconds: [22500]
          },
          {
            Day: "Tuesday",
            Seconds: [3600, 28800, 54000]
          },
          {
            Day: "Wednesday",
            Seconds: [15300]
          },
          {
            Day: "Thursday",
            Seconds: [22500, 39600]
          },
          {
            Day: "Friday",
            Seconds: [18900]
          },
          {
            Day: "Saturday",
            Seconds: [3600]
          }
        ]
      },
      {
        Name: "Kzarka",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/kzarka.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [15300, 39600, 54000]
          },
          {
            Day: "Monday",
            Seconds: [15300, 39600]
          },
          {
            Day: "Tuesday",
            Seconds: [22500]
          },
          {
            Day: "Wednesday",
            Seconds: [15300, 39600, 79200]
          },
          {
            Day: "Thursday",
            Seconds: [22500, 79200]
          },
          {
            Day: "Friday",
            Seconds: [3600, 79200]
          },
          {
            Day: "Saturday",
            Seconds: [28800, 64800]
          }
        ]
      },
      {
        Name: "Offin",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/offin.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [64800]
          },
          {
            Day: "Monday",
            Seconds: [22500]
          },
          {
            Day: "Tuesday",
            Seconds: [28800, 22500]
          },
          {
            Day: "Wednesday",
            Seconds: [3600]
          },
          {
            Day: "Thursday",
            Seconds: []
          },
          {
            Day: "Friday",
            Seconds: [22500]
          },
          {
            Day: "Saturday",
            Seconds: []
          }
        ]
      },
      {
        Name: "Kutum",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/kutum.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [22500, 79200]
          },
          {
            Day: "Monday",
            Seconds: [28800, 67800]
          },
          {
            Day: "Tuesday",
            Seconds: [22500, 79200]
          },
          {
            Day: "Wednesday",
            Seconds: [28800, 54000]
          },
          {
            Day: "Thursday",
            Seconds: [3600, 54000]
          },
          {
            Day: "Friday",
            Seconds: [3600, 39600]
          },
          {
            Day: "Saturday",
            Seconds: [22500, 39600]
          }
        ]
      },
      {
        Name: "Nouver",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/nouver.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [15300]
          },
          {
            Day: "Monday",
            Seconds: [3600, 54000, 79200]
          },
          {
            Day: "Tuesday",
            Seconds: [64800]
          },
          {
            Day: "Wednesday",
            Seconds: [22500, 64800]
          },
          {
            Day: "Thursday",
            Seconds: [28800, 64800]
          },
          {
            Day: "Friday",
            Seconds: [28800, 54000]
          },
          {
            Day: "Saturday",
            Seconds: [22500, 54000]
          }
        ]
      },
      {
        Name: "Garmoth",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/garmoth.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: [3600]
          },
          {
            Day: "Monday",
            Seconds: [22500]
          },
          {
            Day: "Tuesday",
            Seconds: [15300]
          },
          {
            Day: "Wednesday",
            Seconds: []
          },
          {
            Day: "Thursday",
            Seconds: [15300]
          },
          {
            Day: "Friday",
            Seconds: []
          },
          {
            Day: "Saturday",
            Seconds: []
          }
        ]
      },
      {
        Name: "Quint",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/quint.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: []
          },
          {
            Day: "Monday",
            Seconds: []
          },
          {
            Day: "Tuesday",
            Seconds: []
          },
          {
            Day: "Wednesday",
            Seconds: [18900]
          },
          {
            Day: "Thursday",
            Seconds: []
          },
          {
            Day: "Friday",
            Seconds: [64800]
          },
          {
            Day: "Saturday",
            Seconds: []
          }
        ]
      },
      {
        Name: "Vell",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/vell.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: []
          },
          {
            Day: "Monday",
            Seconds: []
          },
          {
            Day: "Tuesday",
            Seconds: []
          },
          {
            Day: "Wednesday",
            Seconds: []
          },
          {
            Day: "Thursday",
            Seconds: []
          },
          {
            Day: "Friday",
            Seconds: []
          },
          {
            Day: "Saturday",
            Seconds: [79200]
          }
        ]
      },
      {
        Name: "Muraka",
        Description: "",
        Image: "https://bdobosstimer.com/img/bosses/muraka.png",
        Time: [
          {
            Day: "Sunday",
            Seconds: []
          },
          {
            Day: "Monday",
            Seconds: []
          },
          {
            Day: "Tuesday",
            Seconds: []
          },
          {
            Day: "Wednesday",
            Seconds: [18900]
          },
          {
            Day: "Thursday",
            Seconds: []
          },
          {
            Day: "Friday",
            Seconds: [64800]
          },
          {
            Day: "Saturday",
            Seconds: []
          }
        ]
      }
    ];
    this.todaysBosses = getNextBoss(this.bosses);
    console.log(this.todaysBosses);
  }
}