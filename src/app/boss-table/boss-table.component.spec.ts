import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BossTableComponent } from './boss-table.component';

describe('BossTableComponent', () => {
  let component: BossTableComponent;
  let fixture: ComponentFixture<BossTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BossTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BossTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
