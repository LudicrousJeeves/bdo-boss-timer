import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-boss-table',
  templateUrl: './boss-table.component.html',
  styleUrls: ['./boss-table.component.css']
})
export class BossTableComponent implements OnInit {
  now: any;
  timeCheck: any;
  weekDays: any;
  selectedTime: any;
  serverBossRotation: any;
  today: any;

  constructor(private dataService: DataService) {
    this.selectedTime = dataService.naUTCTimes;
    setInterval(() => {
      this.now = new Date().toLocaleTimeString();
      }, 1);
      this.today = new Date().getDay();
      this.weekDays = dataService.daysOfTheWeek;
  }

  // getActive(){
  //   let day = this.dataService.getTimeOfDay(this.timeCheck);
  //   let time = this.today;
  //   let id = "id" + day + time;
  //   document.getElementById(id).className = 'table-info';
  // }

  isArray(item){
    return Array.isArray(item);
  }

  ngOnInit(): void {
  }
}