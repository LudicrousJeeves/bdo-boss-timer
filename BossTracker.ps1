$discordUrl = "discordurl"

$todayInSeconds = (Get-Date).TimeOfDay.TotalSeconds
$dayOfWeek = (Get-Date).DayOfWeek
$totalSeconds = 86400

$noBossString = "Take it easy champ. There are no bosses for now."

$weeklyBosses = @(("Karanda", "Karanda", "Kzarka", "Kzarka", "Offin", "Kutum", "Nouver", "Kzarka", ""),
("Karanda", "Kutum", "Kzarka", "Nouver", "Kutum", "Nouver", "Karanda", "Garmoth", ""),
("Kzarka", "Karanda", "", "Karanda", "Nouver", "Kutum", "Offin", "Kzarka", "Quint"),
("Nouver", "Kutum", "Kzarka", "Kutum", "Nouver", "Kzarka", "Kutum", "Garmoth", ""),
("Kzarka", "Nouver", "Karanda", "Kutum", "Karanda", "Nouver", "Kzarka", "Kzarka", ""),
("Karanda", "Offin", "Nouver", "Kutum", "Nouver", "Quint", "Kzarka", "", ""),
("Kutum", "Kzarka", "Kutum", "Nouver", "Kzarka", "Vell", "Garmoth", "Kzarka", "")) 

$dailyTimers = @(900, 7200, 18000, 32400, 43200, 57600, 68400, 80100, 83700)

function GetBoss($dayOfWeek)
{
    switch($dayOfWeek)
    {
        "Monday" { return CurrentBoss(0) }
        "Tuesday" { return CurrentBoss(1) }
        "Wednesday" { return CurrentBoss(2) }
        "Thursday" { return CurrentBoss(3) }
        "Friday" { return CurrentBoss(4) }
        "Saturday" { return CurrentBoss(5) }
        "Sunday" { return CurrentBoss(6) }
    }
}


function CurrentBoss($day)
{
    for($i=0; $i -lt $dailyTimers.Length; $i++)
    {
        if($todayInSeconds -ge $dailyTimers[$dailyTimers.Length - 1])
        {
            return $weeklyBosses[$day + 1][0]
        }
        if($dailyTimers[$i] -ge $todayInSeconds)
        {
            return $weeklyBosses[$day][$i]
        }
    }
}

function GetTime()
{
    for($i=0; $i -lt $dailyTimers.Length; $i++)
    {
        if($todayInSeconds -ge $dailyTimers[$dailyTimers.Length -1])
        {
            return (($totalSeconds - $todayInSeconds) + $dailyTimers[0])
        }
        if($dailyTimers[$i] -ge $todayInSeconds)
        {
           return ($dailyTimers[$i] - $todayInSeconds)
        }
    }
}

$timeDiff = GetTime
$time = New-TimeSpan -Seconds $timeDiff
$boss = GetBoss($dayOfWeek)
$color = 1815124

if($boss -ne "")
{
    $alert = $boss + " is in " + $time.Hours + " hours and " + $time.Minutes + " minutes"    
    $thumbnailBoss = (Get-Culture).TextInfo.ToLower($boss)
    $thumbnailObject = [PSCustomObject]@{
    url = "https://bdobosstimer.com/img/bosses/" + $thumbnailBoss + ".png"
}
    $embedObject = [PSCustomObject]@{
    title = $boss
    description = $alert
    color = $color
    thumbnail = $thumbnailObject
}
}
else
{
    $alert = $noBossString
    $embedObject = [PSCustomObject]@{
    title = $boss
    description = $alert
    color = $color
}
}

[System.Collections.ArrayList]$embedArray = @()
$embedArray.Add($embedObject)

$payload = [PSCustomObject]@{
    username = "BossTracker"
    avatar_url = "https://i.imgur.com/oBPXx0D.png"
    embeds = $embedArray
}

Invoke-RestMethod -Uri $discordUrl -Method Post -ContentType application/json -Body ($payload | ConvertTo-Json -Depth 4)